﻿// 17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <math.h>
class PrintMethod
{
private:
    int a; int b;
public:
    void Print(int a, int b)
    {
        std::cout << a << ' ' << b;
    }
};

class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector(): x(5), y(5), z(5)
    {}
    Vector (double _x, double _y, double _z) : x(_x),y(_y),z(_z)
    {}
    double Radix(double x, double y, double z)
    {
        double radix = sqrt(x * x + y * y + z * z);
        return radix;
    }
    void Print()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
        
    }
};

int main()
{
    PrintMethod Examp;
    Examp.Print(2, 5);
    Vector v;
    v.Print();
    std::cout << '\n'<<"Module: "<< v.Radix(2,5,6);
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
